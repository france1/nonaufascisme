.. index::
   pair: Falscisme ; Fascisme


.. _falscisme_groupe_jp_vernant_2019_10_17:

========================================================================
**Falscisme**
========================================================================

.. seealso::

   - http://www.groupejeanpierrevernant.info/#Falscisme
   - http://kpolanyi.scoolaid.net:8080/xmlui/handle/10694/565
   - https://www.monde-diplomatique.fr/cartes/vote-revenu-FN
   - https://laviedesidees.fr/Front-national-un-parcours-sinueux.html
   - https://laviedesidees.fr/Karl-Polanyi-le-marche-et-le.html


.. contents::
   :depth: 3


Citations
============

Françoise Giroud
------------------

Ainsi commence le fascisme. Il ne dit jamais son nom, il rampe, il flotte,
quand il montre le bout de son nez, on dit : C'est lui ? Vous croyez ?
Il ne faut rien exagérer ! Et puis un jour on le prend dans la gueule et il
est trop tard pour l'expulser.”

Françoise Giroud

Karl Polanyi, 1934
---------------------

By denouncing Socialism and Capitalism alike as the common offspring of
Individualism, it enables Fascism to pose before the masses as the sworn
enemy of both.
The popular resentment against Liberal Capitalism is thus turned most effectively
against Socialism without any reflection on Capitalism in its non-Liberal, i.e.
corporative, forms.
Though unconsciously performed, the trick is highly ingenious.

First Liberalism is identified with Capitalism ; then Liberalism is made to
walk the plank; but Capitalism is no worse for the clip, and continues its
existence unscathed under a new alias.”

Karl Polanyi, 1934 [1_]

Introduction à l'article
===========================


L’annonce de la suspension de nos activités nous a valu de nombreux courriers,
très chaleureux, nous en demandant les raisons.
La raison la plus profonde tient au moment politique que nous traversons,
annonciateur de gros temps pour une décennie au moins, et sans doute plus.

Il devient urgent de nous y préparer, ce qui nécessite d’y consacrer du temps —
le bien le plus précieux dont nous disposions encore. Ayant peu de goût pour
le catastrophisme, il nous a semblé important de rassembler dans ce billet des
éléments analytiques sur la crise politique en cours et en particulier sur
la nature des régimes qui, des États-Unis à la Hongrie en passant par le Brésil,
l’Inde ou les Philippines, menacent l’autonomie de la recherche et de l’Université.

La crise politique française peut se résumer à la partition du champ politique
en trois blocs: le bloc modernisateur au pouvoir, qui mène une politique
néolibérale, un bloc nationaliste et identitaire, issu de la normalisation
de la vieille extrême-droite [2_], et un bloc émancipateur fragmenté, qui tente
de reconstruire un projet social intégrant la nécessité de juguler le
réchauffement climatique et l’effondrement des espèces.

Aucun de ces trois blocs n’est capable de s’assurer le soutien d’une majorité
de la population.
Seules, les institutions de la cinquième République ont permis à un parti
comme LREM, disposant d’une base de 11% de l’électorat [3_], d’obtenir 53% des
sièges à l’Assemblée nationale, ce qui oblige ce bloc modernisateur à avoir
recours à une répression violente et autoritaire du mouvement social pour pallier
l’absence d’adhésion à son projet néolibéral  [4_].



Allons à l’évidence
====================

Allons à l’évidence : les politiques économiques et sociales menées aux Brésil,
en Hongrie, en Italie ou États-Unis sont indubitablement néolibérales
(dérégulation financière [12], flat tax [13], suppression de droits des salariés
(loi Travail) [14], privatisations, réduction des aides sociales, etc.)

Il n’y a là aucune coïncidence, mais une filiation intellectuelle explicite de
l’entourage des présidents de ces États avec les penseurs néolibéraux de
l’école Autrichienne (Ludwig von Mises, Friedrich von Hayek, etc.) comme de
l’École de Chicago (Milton Friedman, etc).

Si les courants de l’Alt-right se distinguent par des nuances quant aux
questions de mœurs et quant aux références évangéliques (néoconservateurs)
ou technophiles (libertariens), ils se rejoignent sur l’essentiel en promouvant
une **hybridation entre néolibéralisme et fascisme**, alliant le culte de la
libre concurrence au nationalisme, à la xénophobie, à l’eugénisme,
au climato-négationnisme, au suprémacisme blanc, à la haine de l’altérité et
au rejet des “droits civiques”.

**Cette hybride monstrueux hérite du néolibéralisme l’idée d’un État illibéral**,
réduit à sa fonction répressive, au service du marché et sous le contrôle de
la sphère actionnariale.
Comme n’a cessé de le répéter Robert Paxton, ceci constitue une différence
fondamentale avec l’État planificateur et centralisé des fascismes historiques,
adossés à la Nation comme communauté organique transcendant tout individualisme [15].

S’il s’agit toujours de détruire l’institution de la société comme communauté
de personnes, **cela ne passe plus comme dans les totalitarismes par la disparition
de l’individu mais par sa privatisation**.

La résurrection métastatique du mythe identitaire — la race, le sang, le chef —
n’est plus affaire de masse mais d’atomisation sociale, de séparation avec
l’expérience sensible.

*F. Hayek et son Road to Serfdom* servent ainsi de référence centrale à M. Philippe
(dans la traduction de M. Barre) [16] comme à M. Bannon, qui ne cesse de le
citer dans les meetings auxquels il participe à travers l’Europe [17].

M. Trump et M. Salvini vénèrent M. Laffer, le théoricien de la réduction
d’impôts [18] **qui a inspiré la suppression de l’ISF portée par M. Macron**.

M. Salvini se réfère également à Mme Thatcher, pour sa conception de la liberté
réduite à la liberté économique, et à Alvin Rabushka, un politiste affilié aux
think tanks néolibéraux Hoover Institute et Société du Mont-Pélerin, pour la
flat tax. [19]

Au Brésil, M. Bolsonaro est entouré de **Chicago boys** comme le ministre des
finances, M. Guedes, néolibéral influencé par Milton Friedman, qui a enseigné
l’économie au Chili pendant la dictature de M. Pinochet [20].

Partisan d’un Brexit dur, M. Johnson s’est entouré de “libertariens”
revendiqués comme M. Javid (Chancellor), M. Raab (Foreign office), Mme Patel
(Home office), ou Mme Truss (International Trade Secretary), auteurs du pamphlet
“Brittania Unchained” [21].

Il en va de même pour l’AfD allemand ou le FPÖ autrichien, deux partis
postfascistes et ordo-libéraux dont des dirigeants importants ont été formés
dans les think tanks néolibéraux (Société du Mont-Pélerin, Hayek Society, etc) [22].


Comment le protectionnisme nationaliste peut-il être compatible avec la dérégulation néolibérale ?
===================================================================================================

Comment le protectionnisme nationaliste [27] prôné par le bloc identitaire peut-il
être compatible avec la dérégulation néolibérale ?

Si les institutions européennes — ainsi que d’autres institutions internationales
comme le Fonds monétaire international, la Banque mondiale ou l’Organisation
mondiale du commerce — ont été utilisées pour imposer, contre les souverainetés
étatiques, des réformes néolibérales et unifier les marchés à l’échelle planétaire,
cela n’implique nullement un rejet des frontières, puisque **celles-ci permettent
de mettre en concurrence les États**.

Lorsque M. Trump utilise la puissance coercitive de l’État fédéral américain,
c’est pour contraindre les autres nations à se conformer à la logique de marché.

Les théories économiques néolibérales s'accommodent ainsi parfaitement des
nationalismes comme du “globalisme”.
Exemple révélateur, le Brexit a été pensé dans les cercles néolibéraux, dès la
fin des années 1980, sous l’influence de Mme Thatcher [28].

La campagne pour le Brexit a du reste été influencée par M. Mercer, propriétaire
“libertarien” de Cambridge Analytica [29] et financeur le plus important de la
campagne de M. Trump, lui permettant d’imposer M. Bannon à ses côtés.
L’opération a été épaulée par M. **Thiel, fondateur de Paypal et de Palantir,
soutien lui aussi de M. Trump et spécialiste des techniques de surveillance,
de fichage et de contrôle des populations** [30] .

Ces deux hybrides du néolibéralisme, l’un managérial et l’autre nationaliste,
partagent non seulement les grandes lignes des orientations économiques, mais
aussi les mêmes boucs émissaires à sacrifier.

Rompant avec les compromis consentis par les détenteurs du capital financier
pendant l’ère fordiste, **l’ère néolibérale a méthodiquement remis en cause
l’ensemble des systèmes de solidarité qui assuraient des formes de sécurité
aux classes moyennes et populaires (école, santé, chômage, retraite)**.

Dès lors, comment la sphère politique doublement soumise au système représentatif
et au contrôle des investisseurs peut-elle répondre à la demande de sécurité
économique, politique et culturelle des classes moyennes ?
Par “l’organisation concertée de toutes les petites peurs, de toutes les petites
angoisses” [31], ce qui suppose la surexposition aux questions migratoires
et identitaires, jouant d’un imaginaire colonial encore vif [32].

Les tenants des deux néolibéralismes usent ainsi du même renversement de la
rhétorique des révolutions française et américaine d’abolition des privilèges
de l’ancien monde par un ordre nouveau : les “privilégiés” dont le sacrifice
est supposé mettre fin au déclin ne sont plus les aristocrates mais les chômeurs
(fainéants), les fonctionnaires (rentiers), les militants climatiques (liberticides)
et surtout, **les migrants** [33].

Le néolibéralisme identitaire, nationaliste, xénophobe et climato-négationniste
est donc la réaction du néolibéralisme à sa propre crise, et en est probablement
la phase terminale.

Nous renvoyons les lecteurs à l’analyse de Karl Polanyi [34], qui a montré
comment le mythe d’un marché auto-régulateur a conduit à un processus de
désencastrement de l’économie des institutions sociales et politiques, à une
crise profonde du libéralisme et **in fine à la montée des fascismes**.

Nous y sommes à nouveau et il y a si peu à ajouter à La Grande Transformation.


L’hybride managérial et globaliste et l’hybride identitaire et nationaliste
===============================================================================

L’hybride managérial et globaliste et l’hybride identitaire et nationaliste du
néolibéralisme diffèrent fondamentalement par les rhétoriques déployées,
qui s’adressent à des blocs sociaux distincts.

Le premier s’adresse à la classe moyenne supérieure, en appréciant son capital
culturel par un discours fondé sur la modération, sur l’idée d’expertise, sur
la notion galvaudée de "progressisme", sur l’adaptation nécessaire à un monde
qui change.
Il use de storytelling et d’énoncés ouverts, euphémisés, cotonneux, à la
logique évanescente, niant tout antagonisme, pour fabriquer le consentement.

Le second apprécie le capital d’autochtonie de la classe moyenne en colère en
en reprenant les codes culturels : casquette de baseball pour M. Trump,
Nutella pour M. Salvini [35], spectacle clownesque pour M. Johnson.

Il a recours au clash, à l’Ubuesque, au grotesque, en visant avant tout à
susciter l’offuscation morale des “bien-pensants”.
Plus le leader se trouve disqualifié par l’infâme, l’odieux, le ridicule, et
plus le spectateur jouit de se reconnaître dans la figure incarnée par le
souverain arbitraire, en une passion triste qui lui fait désirer son
propre asservissement [36].


Le *falscisme*
==================

La dernière voie d’action de l’extrême-droite “libertarienne” est la promotion
active d’une Alt-culture réduisant la science à la “technologie” et à la
recherche de “solutions”, contaminant la recherche par l’entreprenariat et la
prostitution intellectuelle.
Les **conférences TED et le Media Lab du MIT** en sont les exemples paradigmatiques [50_].

L’investissement massif des Tech-milliardaires “libertariens” pour constituer
des réseaux de promotion de leurs idées ambitionne de priver la science de sa
visée collective et désintéressée de dire le vrai sur le monde.

Cette entreprise de démolition de toute éthique intellectuelle et de toute norme
de véridiction, accompagnée d’une valorisation du conflit d’intérêt comme
norme positive, a ceci de dangereux qu’elle use du retournement du réel en
reprenant à son compte la rhétorique du progrès et de la raison.

**Sa nature profonde suggère d’introduire un mot-valise pour nommer cette phase
terminale du néolibéralisme : falscisme**.


Post-scriptum, en forme de cauchemar
=========================================

Si l’extrême-droite “libertarienne” est associée de longue date au
climato-négationnisme, et use de tout son pouvoir de nuisance pour déprécier
les militants climatiques, elle prétend également détenir la solution au
réchauffement climatique, qui mêle eugénisme, racisme et malthusianisme — d’où
son obsession à prétendre à une origine génétique de l’intelligence, associée
à une héritabilité du QI [51_].

Il suffirait, lit-on dans les publications transhumanistes de cette mouvance,
de réduire l’humanité de moitié, en sélectionnant les personnes intelligentes
donc adaptables.

L’urgence climatique étant à l’échelle de deux décennies, un simple chiffrage
de cette “solution” en ordre de grandeur nous donne un demi million de morts
par jour.


Liens
========

.. _1:

[1]
-----

Dénoncer pareillement le socialisme et le capitalisme comme la descendance
commune de l’individualisme permet au fascisme de se poser devant les masses
comme l’ennemi juré des deux.
Le ressentiment populaire contre le capitalisme libéral est ainsi détourné
très efficacement contre le socialisme sans aucune répercussion sur le
capitalisme dans ses formes non libérales, c.a.d. corporatives.
Quoique inconsciemment réalisé, la ruse est très ingénieuse.
D’abord le libéralisme est identifié avec le capitalisme ; ensuite le
libéralisme est mis au pilori ; mais le capitalisme en sort indemne et
continue son existence, préservé sous un nouvel habillage."

Karl Polanyi “The Essence of Fascism” - Christianity and the Social Revolution, 1933-1934, pp. 359-394
http://kpolanyi.scoolaid.net:8080/xmlui/handle/10694/565



.. _2:

[2]
-----

Palheta, Ugo La possibilité du fascisme. France, la trajectoire du désastre.
La Découverte, 2019.

On y trouve par exemple cette citation :
"On a tout intérêt à pousser le FN.
Il rend la droite inéligible. Plus il sera fort, plus on sera imbattables.
C'est la chance historique des socialistes."
Pierre Bérégovoy, ministre des Affaires sociales et de la Solidarité nationale, juin 1984.

Violaine Girard, Le Vote FN au village. Trajectoires de ménages populaires du
périurbain, Éditions du Croquant, 2017
Recension de l'ouvrage : https://journals.openedition.org/lectures/23814

Valérie Igounet, Le Front national de 1972 à nos jours. Le parti, les hommes, les idées, Seuil.
Recension de l'ouvrage: https://laviedesidees.fr/Front-national-un-parcours-sinueux.html

Quelques clichés erronés sur le Front National:  https://www.monde-diplomatique.fr/cartes/vote-revenu-FN


.. _3:

[3]
----

Les élections européennes permettent de mesurer l’adhésion au projet modernisateur
néolibéral : environ 11% des inscrits, avec 51% d’abstention.
Cela coïncide avecla taille du groupe social issu des classes moyennes
supérieures, qui assure la base électorale de ce projet.

.. _4:

[4] Romaric Godin, La Guerre sociale en France
-------------------------------------------------

Romaric Godin, La Guerre sociale en France Aux sources économiques de la
démocratie autoritaire, éditions La Découverte, 2019.


.. _9:

[9] “Populisme” , “Néo-fascisme”
----------------------------------

Nous proposons ici quelques références commentées sur ces dénominations
problématiques. On trouvera une liste plus complète dans cet article de revue :

Mabel Berezin, “Fascism and Populism : Are They Useful Categories for Comparative
Sociological Analysis?” Annual Review of Sociology, 45 2019. https://www.annualreviews.org/doi/abs/10.1146/annurev-soc-073018-022351


“Populisme” est le plus souvent utilisé pour disqualifier l’aspiration
démocratique des classes moyennes. Le lecteur désireux de prendre connaissance
des lieux communs en la matière pourra lire cet ouvrage :
**Diamanti, Ilvo et Lazar, Marc. Peuplecratie. La métamorphose de nos démocraties, Gallimard, 2019**.

Sur la vision d’un affrontement entre démocratie illibérale et néolibéralisme
autoritaire et antidémocratique, quelques ouvrages:

- Wendy Brown Défaire le démos. Le néolibéralisme, une révolution furtive, Amsterdam, 2018.
- Yasha Mounk The People vs. Democracy Why Our Freedom Is in Danger and
  How to Save It, Harvard University Press, 2018.
- Dani Rodrik The Globalization Paradox – Democracy and the Future of the
  World Economy, (ed) Norton, 2011.
- Pierre Rosanvallon La Contre-démocratie. Paris, Éditions du Seuil, 2006, 345 p.

“Néo-fascisme” est le plus souvent utilisé pour désigner des mouvements se
référant explicitement au régime fasciste italien, comme “néo-nazi” pour
le nazisme. Le qualificatif le plus précis est le mot “post-fascisme”,
défendu par Enzo Traverso:

- Enzo Traverso, Les nouveaux visages du fascisme, Paris, Textuel, 2016.
- Spectres du fascisme par Enzo Traverso:  https://www.cairn.info/revue-du-crieur-2015-1-page-104.htm
- Fascisms Old and New, an interview with Enzo Traverso
  https://jacobinmag.com/2019/02/enzo-traverso-post-fascism-ideology-conservatism


.. _10:

[10] Sur la nature des controverses dans l’Histoire du fascisme
-------------------------------------------------------------------

Sur la nature des controverses dans l’Histoire du fascisme, on pourra lire :

- Roger Griffin. “« Consensus ? Quel consensus ? » Perspectives pour une
  meilleure Entente entre spécialistes francophones et anglophones du fascisme”.
  Vingtième Siècle. Revue d'histoire 2010/4 (n° 108).
  https://www.cairn.info/revue-vingtieme-siecle-revue-d-histoire-2010-4-page-53.htm

Nous proposons une courte bibliographie sur le fascisme historique:

- Roger Eatwell, Fascism, Londres, Chatto and Windus, 1996.
- Emilio Gentile Qu'est-ce que le fascisme ? : Histoire et interprétation
  [« Fascismo. Storia e interpretazione »], Folio, coll. « Histoire », 2004.
- Roger Griffin, The Nature of Fascism, Londres, Pinter, 1991.Robert O. Paxton
  The Anatomy of Fascism, 2004.
- George L. Mosse, The Fascist Revolution : Toward a General Theory of Fascism,
  New York, Howard Fertig, 1999.
- Stanley Payne, A History of Fascism : 1914-1945, Londres, UCL Press, 1995.
- Zeev Sternhell Ni droite ni gauche. L'idéologie fasciste en France,
  Collection Folio histoire (n° 203), Gallimard

.. _11:

[11] Eco, Umberto (1995). Ur Fascisme.
---------------------------------------

Eco, Umberto (1995). Ur Fascisme: https://www.pegc.us/archive/Articles/eco_ur-fascism.pdf


.. _12:

[12]
------

Il est par exemple significatif que M. Bolsonaro ait prononcé le discours
d’ouverture du forum de Davos https://www.la-croix.com/Economie/Monde/A-Davos-Jair-Bolsonaro-defend-nouveau-Bresil-2019-01-23-1200997416

.. _13:

[13] “flat tax”  de Salvini et réformes de M. Macron
---------------------------------------------------------

La même mesure de “flat tax” se retrouve dans l’Italie penta-fasciste de M. Salvini

- https://www.mediapart.fr/journal/international/040618/en-italie-la-flat-tax-ancre-bien-droite-le-nouveau-gouvernement

et dans les réformes de M. Macron :

- https://www.lemonde.fr/idees/article/2017/10/25/la-flat-tax-est-une-bombe-a-retardement-pour-les-finances-publiques_5205612_3232.html

Les réformes de M.Trump comportent le même type de “tax cuts” inspirée par
M. Laffer, ainsi que le programme de M. Johnson, en Angleterre: https://www.ft.com/content/15a099c2-9699-11e9-8cfb-30c211dcd229

En particulier, M. Trump a divisé par deux la fiscalité sur le profit des entreprises: https://aneconomicsense.org/2019/03/28/taxes-on-corporate-profits-have-continued-to-collapse/


.. _14:

[14] Hungary passes 'slave law' prompting fury among opposition MPs.
-------------------------------------------------------------------------

Hungary passes 'slave law' prompting fury among opposition MPs:  https://www.theguardian.com/world/2018/dec/12/hungary-passes-slave-law-prompting-fury-among-opposition-mps


.. _50:

[50] We need to talk about TED.
---------------------------------

- https://www.theguardian.com/commentisfree/2013/dec/30/we-need-to-talk-about-ted

Culture numérique pour milliardaires dégénérés: https://blog.mondediplo.net/culture-numerique-pour-milliardaires-degeneres

How Rich Donors Like Epstein (and Others) Undermine Science : https://www.wired.com/story/the-problem-with-rich-people-funding-science/

Jeffrey Epstein's influence in the science world is a symptom of larger problems: https://www.theguardian.com/commentisfree/2019/aug/27/jeffrey-epstein-science-mit-brockman

The Problem With Sugar-Daddy Science: https://www.theatlantic.com/ideas/archive/2019/09/problem-sugar-daddy-science/598231/


.. _51:

[51] Stephen Jay Gould (1981) The Mismeasure of Man.
-----------------------------------------------------

Recension de l'ouvrage: Race and intelligence: A sorry tale of shoddy science: https://www.theguardian.com/science/2009/nov/12/race-intelligence-iq-science

How science has shifted our sense of identity: https://www.nature.com/articles/d41586-019-03014-4

Julien Larregue, 2018 « C’est génétique » : ce que les twin studies font dire
aux sciences sociales. Sociologie 3, 9. : https://journals.openedition.org/sociologie/3526

The unwelcome revival of ‘race science’ : https://www.theguardian.com/news/2018/mar/02/the-unwelcome-revival-of-race-science

Le gourou de la génétique comportementale est Robert Plomin, dont on pourra
circonscrire les menées en lisant cet article de revue:
R.Plomin and S. von Stumm (2018) The new genetics of intelligence. Nature Reviews
Genetics 19, 148–159. https://www.nature.com/articles/nrg.2017.104

Le meilleur livre apportant les preuves des fondements douteux de la génétique
comportementale:
Aaron Panofsky, Misbehaving Science. Controversy and the Development of
Behavior Genetics, Chicago, University of Chicago Press, 2014
Recension de l'ouvrage: https://journals.openedition.org/sociologie/3165

