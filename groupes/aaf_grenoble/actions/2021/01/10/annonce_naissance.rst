
.. _aaf_naissance_2021_01_10:

========================================================================================
Dimanche 10 janvier 2021 Naissance de l'Action Antifasciste de Grenoble
========================================================================================

.. contents::
   :depth: 3

:download:`naissance_afa_gre.pdf`


Introduction
===============================================

Pourquoi former un groupe antifasciste, à Grenoble, aujourd'hui ?

Les différentes crises qui ont été traversées ces dernières années ont un
effet considérable sur l'État français et ses institutions:

**Le racisme d'État**, dont l'islamophobie est la partie la plus visible,
atteint des sommets dignes de l'époque coloniale, avec le récent projet
de loi "contre les séparatismes" qui vise directement les musulmanes et
musulmans et ne rencontre pas encore assez de résistance.

**L'État et ses institutions (notamment la police et la justice) traquent sans répit
les sans-papiers** et les enferment dans des centres de rétentions
administratifs, à l'abri des regards.

Pendant ce temps, les médias diffusent massivement des idées racistes, sans
que celles-ci soient contredites.

Le racisme aujourd'hui ne provient donc pas que du Rassemblement National
(anciennement front national) et des groupes d'extrême droite.

C'est aussi et avant tout le gouvernement, les institutions de l'État
et les discours des médias qui le diffusent tous les jours.

Cela frappe de manière quotidienne et grave, les personnes qui en sont
les cibles (en premier lieu, les populations noires, les populations
arabes et les populations roms).

Hors de ses frontières, l'État français fait toujours autant de dégâts,
par ses interventions impérialistes (dans le domaine économique, politique
et militaire).

Le pillage des pays d'Afrique, d'Asie et du Moyen-Orient par la France,
est trop peu dénoncé.
Il est nécessaire de s'y opposer afin de combattre le racisme d'État.

En plus de cela, plusieurs lois et décrets attaquent et réduisent régulièrement
nos libertés (loi sécurité globale, Loi programmation recherche , fichage
massif...) et donnent toujours plus de pouvoir aux forces de l'ordre.
Ces mêmes forces de l'ordre qui, tous les jours, mutilent, tuent, violent
et se font le relais le plus violent du racisme d'État.

Berthold Brecht disait "le fascisme n'est pas le contraire de la
démocratie, mais son évolution en temps de crise".

Comment s'organiser ?
=======================

La politique de La République en Marche suit un objectif : présenter
en 2022, un choix qui paraîtrait inévitable : le fascisme de Marine Le Pen
ou le libéralisme autoritaire et raciste d'Emmanuel Macron.

Pour nous, la solution, afin de ne pas se retrouver dans cette impasse,
réside dans la capacité du peuple à s'auto-organiser et à changer les
choses par lui-même et pour lui-même, au lieu de s'en remettre à des
leaders politiques.

À Grenoble, des collectifs et des individus s'organisent et luttent dans
le sens de l'émancipation et de l'auto-organisation populaire.

Que ce soit par la lutte pour des conditions de logement dignes, pour la
régularisation des demandeurs de papiers, par les combats syndicaux, par
la solidarité populaire, par les luttes des habitants et habitantes des
quartiers populaires, des personnes LGBTQI+ ou les luttes féministes.

Ces initiatives nous montrent la voie.

Nous souhaitons être à leurs côtés et les soutenir, sur le terrain.

De manière humble, nous sommes désireux, désireuses et cainvaincu.e.s
que c’est grâce aux échanges avec les personnes, premières concernées,
que nous lutterons !

Nous ne voulons en aucun cas devenir un parti politique ou chercher à
acquérir un quelconque pouvoir.
Nous voulons soutenir toute initiative populaire qui vise à mettre du
pouvoir dans les mains des personnes qui composent aujourd'hui
notre classe : la classe de celles et ceux qui ne possèdent pas de
quoi s'enrichir sur le travail des autres.

À côté de cela, nous souhaitons être à l'initiative de rencontres et
d'évènements larges et populaires afin de partager notre vision d'un
antifascisme qui vise aussi l'État et ses institutions.

Il est aussi question de mener le combat au niveau des idées, pour
contrer les opinions d'extrême droite largement répandues aujourd'hui.

Ceci s'éloigne de la vision qu'on peut avoir d'un mouvement antifasciste
qui s'en tiendrait à la lutte contre l'extrême-droite.

C'est une redéfinition des priorités dont nous sommes convaincu.e.s et
pour laquelle nous nous coordonnons avec d'autres groupes antifascistes,
en France et en Europe.

À Grenoble, nous ne voulons en aucun cas revendiquer un monopole de
l'antifascisme.
Nous lutterons avec toute personne ou collectif qui porte des initiatives
cohérentes avec nos mots d'ordre.

Pour éviter les suppositions hasardeuses, nous souhaitons préciser que
ce collectif est aussi composée de meufs qui ne souhaitent pas être
invisibilisées au travers de clichés véhiculés.

À bientôt dans nos luttes !
Grenoble, le 10 janvier 2021.
