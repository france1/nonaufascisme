
.. index::
   pair: CLAF; 2014 (23 Avril)

.. _claf_23_avril_2014:

=========================================================
Face au fascisme: riposte sociale ! (CLAF 23 avril 2014) 
=========================================================

.. seealso::

   - http://www.c-g-a.org/sites/default/files/4_pages.pdf
   - http://fr.slideshare.net/assr38/claf-2014-riposte-sociale-antifasciste


.. contents::
   :depth: 3


Format PDF lourd (27 Mo)
=========================

.. seealso::

   - http://www.c-g-a.org/sites/default/files/4_pages.pdf



PDF léger (110 Ko)
===================

.. seealso::

   - http://www.cnt-f.org/ul38/wp-content/uploads/Tract_claf_txt_23_avril_2014.pdf


Rappel
======

L'assassinat du jeune militant antifasciste, Clément Méric, le juin 5 2013 à 
Paris a médiatisé la violence des groupes fascistes en lien avec leur résurgence. 

Une réalité malheureusement bien trop connue par celles et ceux qui luttent au 
quotidien et sur le terrain contre le retour de la peste brune et plus 
généralement le développement et la banalisation des idées racistes, xénophobes, 
sexistes, patriarcales, homophobes, réactionnaires et autoritaires contenues et 
portées par le fascisme sous ces différentes expressions.

L'extrême droite essaie de lisser son image et de séduire les travailleurs mais 
elle reste du côté des exploiteurs et continue de défendre un projet de société 
fondé sur l'exclusion, la soumission et la répression. 
Derrière des discours pseudo-sociaux et populaires se cachent en effet des 
idées réactionnaires et autoritaires.

Un discours raciste, machiste et homophobe
==========================================

Patriarcat et homophobie 
-------------------------

Pour l'extrême droite, les femmes devraient retourner à la maison et obéir au 
mari, et l’IVG ne devrait pas exister. 

Les homos, lesbiennes, bisexuel-lle-s ou transexuel-lle-s seraient "contre-nature" 
et ne devraient pas disposer des mêmes droits que les hétéros.

Racisme
--------

L'extrême droite veut supprimer le droit du sol, les allocations aux familles 
dont au moins l’un des parents n’est pas français, et expulser tous les 
sans-papiers, comme s'ils étaient responsables des méfaits du capitalisme.

Religion 
---------

Leur prétendue défense de la laïcité n'est qu'un racisme masqué. Ils ne se 
servent de ce concept que dans un but : affirmer la primauté de leurs idées au 
détriment de toutes les autres…

Violence et autoritarisme
===========================

Etatisme autoritaire 
---------------------

Le fascisme c’est le règne du grand capital allié un Etat répressif chargé de 
faire régner l’ordre au profit d’une classe de profiteurs.

Culte du chef
------------- 

La société d'extrême droite, c'est une société pyramidale où  les chefs décident 
de tout, s’accaparent toutes les richesses et réduisent au silence les dissidents. 
L’Etat est là pour encadrer l'exploitation.

La violence comme mode de gouvernement 
---------------------------------------

Pénalisation, répression, toutes les armes de l'Etat sont mises au service de 
la domination. 
C’est la haine de ceux qui vivent différemment, la guerre et le flicage : 
multiplication des caméras de surveillance, augmentation du budget de l’armée, 
création d’une milice.


Un discours fondé sur la peur et la haine
-----------------------------------------

Dans un contexte de crise, d’inégalités, de chômage et de précarité grandissante, 
taper sur les immigrés, ceux et celles qui sont “différents”, permet à 
l’extrême droite et à la bourgeoisie de dévier la colère populaire sur des boucs 
émissaires : les immigré-e-s, les internationalistes « qui les soutiennent et 
se moquent des frontières », les syndicalistes «complices de la mondialisation 
du travail» et les associations LGBT (Lesbiennes-Gays-Bis-Trans) qui menaceraient 
l’ordre moral de la société. 

Après plus de vingt ans de diffusion des idées du Front national, celles-ci 
sont de plus en plus reprises par l'ensemble de la classe politique. 
Les relents islamophobes et antisémites actuels tout comme les déferlements 
réactionnaires et homophobes lors des mobilisations contre le mariage pour tous 
en sont les plus tristes exemples.

Ils tentent de se donner une image anticapitaliste, alors qu'ils prennent 
systématiquement le parti des patrons lors des mouvements sociaux, au nom de 
la « défense des intérêts de la nation ». 

Ils font donc le jeu des patrons en divisant la classe des opprimés quand ils 
ne servent pas de gros bras pour briser les luttes. 
Ces mouvements s'appuient aussi sur le vieux fond de nationalisme présent dans 
quasiment tous les courants politiques en mettant en avant un capitalisme 
protectionnisme faussement opposé au « capitalisme financier ».

La chasse aux sans-papiers, la fermeture des frontières, y compris pour les 
demandeurs d’asile, représentent ainsi pour l’extrême droite la solution 
miracle à tous les maux. 

Elle nie ainsi que ce sont les patrons, les actionnaires, les banquiers qui 
exploitent les salarié-e-s avec l'aide des politiciens et des gouvernements de 
tout bord. L'Etat reste toujours au service du capital.

Un discours défendu par la violence
====================================


Les dirigeants de l’extrême droite dénoncent «l’insécurité» à la télé. 
Pourtant son fonds de commerce c’est la violence. Le FN entretient des 
relations avec un tas de groupuscules qui font régner la terreur contre 
celles et ceux qu’ils désignent comme des ennemis intérieurs : juifs, 
rroms, arabes, homosexuel-lle-s, etc. 

Les agressions se multiplient. Mais également contre celles/ceux qui 
contestent le capitalisme : militant-e-s associatifs, syndicaux ou 
politiques sont régulièrement agressé-e-s voir assassiné-e-s.

Un discours antiféministe et homophobe 
======================================

L’extrême droite a une conception réactionnaire et antiféministe de la place 
des femmes dans la sphère publique comme privée. 
L’idée générale est de renvoyer les femmes au foyer, les reléguant ainsi au 
rang de poule pondeuse destinées à élever les enfants et servir fidèlement 
leur époux. 
Le contrôle du corps des femmes passe par une remise en question de l’IVG et 
de la contraception libre et gratuite.
Dans la même logique, les fascistes et leurs alliés conservateurs nient et 
oppriment tout ce qui ne relève pas des relations hétérosexuelles et 
patriarcales. 
C’est pourquoi ils refusent que les homosexuels, les lesbiennes, les bisexuel-lle-s 
et les transexuel-le-s disposent des mêmes droits que les hétéros. 
La lutte contre le mariage pour toutes et tous en est l’un des exemples les 
plus récents.

Un discours autoritaire et répressif
====================================

Pour l'extrême droite, il n'y a qu'une seule solution à tous les problèmes 
sociaux : les coups de bâton. 

Tout ce qu'elle propose pour répondre à la détresse sociale des quartiers 
populaires, c'est le couvre-feu et la répression. 
Et comme si les crimes policiers n'étaient déjà pas assez nombreux, le FN 
voudrait accorder aux forces de l'ordre une "présomption de légitime défense", 
autant dire un permis de tuer. 
Le même discours autoritaire vaut pour l'école où les seuls remèdes avancés 
pour l'échec scolaire sont le bourrage de crâne et les humiliations collectives, 
la soumission à l'école préparant à la soumission à l'entreprise. 

L'extrême droite ne propose pas de changer de système mais simplement de 
verrouiller la société par la répression et la violence d'Etat.

L’extrême droite est capitaliste et antisyndicale.

L’extrême droite critique le libéralisme européen et mondial. 
L’extrême droite prétend défendre ceux et celles qui triment au quotidien pour 
s’en sortir. Ce n’est que mensonge et hypocrisie. 

Le FN est un parti capitaliste dirigé par des millionnaires. 
Les fascistes ne sont pas anticapitalistes, ce qu’ils veulent c’est un 
capitalisme français dans lequel les gens galèreront autant. 
Les patrons qu’ils soient chinois, anglais ou français sont pourtant de la même 
espèce. 

Le projet social des fascistes c’est : le même système mais sans syndicats, 
sans associations pour se défendre. 
L’extrême droite ne fait que diviser les travailleur-ses entre eux faisant le 
jeu des classes dirigeantes.

Conspirationnisme et fascisme
==============================

L'ensemble des groupes conspirationnistes repose sur la même base : "On nous 
cache quelque chose". Mais qui est ce "on" ?  

Il semblerait que ce "on" soit juif, en effet derrière la plupart  des discours 
conspirationnistes des groupes tels que les Illuminatis,  Zeitgeist , Reopen 911 
ou encore les reptiliens lorsque l'on gratte un peu, le lecteur ou la lectrice 
avisé-e décèle vite l’antisémitisme à peine dissimulé des discours. 
C'est donc le si fameux complot juif ! On trouve comme idée récurrente, la 
théorie d'un complot organisé par le groupe Bilderberg ou encore par la 
commission trilatérale, le tout manipulé par ... David Rockfeller ou les 
Rotschilds (dans les milieux conspirationnistes M. Rockfeller, parce que riche 
est considéré comme Juif !).

Des groupes qui se créent des ennemis imaginaires et, ce faisant, se trompent 
de cibles.
Ils s’inventent des conspirations et, plutôt que de s’attaquer au capitalisme, 
ils divisent la classe ouvrière.


Connaître les  principaux groupes nationalistes français
=========================================================

- À quoi ressemble l’extrême droite aujourd’hui ? 
- Quelle est la place du Front national ? 
- Combien de groupes y a-t-il à sa marge, et que représentent-ils vraiment ? 

Pas si facile aujourd’hui de répondre. Face à une extrême droite en perpétuelle 
évolution, cherchant de plus en plus souvent à brouiller les cartes pour mieux 
se refaire une virginité et apparaître plus forte qu’elle ne l’est, il vaut 
mieux connaître les histoires, les alliances et les positionnements de ces 
différents mouvements pour mieux anticiper leurs actions et leurs politiques. 

Vous pouvez vous procurer le schéma du «Petit monde merveilleux de 
l’Extrême-droite» en vous rendant sur le site de REFLEX SITE D’INFORMATIONS 
ANTIFASCISTES : L’extrême droite, mieux la connaitre pour mieux la combattre 
(version 2013)

Vous pouvez aussi vous procurer ces informations  sur le site  de No Pasaran
http://nopasaran.samizdat.net



Quelles alternatives au fascisme et au capitalisme ?
======================================================
    
Le projet libertaire propose des valeurs et une société complètement opposées 
à celles que proposent les fascistes. 

Egalitaire et libertaire, il est fondé sur le partage des richesses et 
l'égalité économique et sociale dans tous les domaines de la vie. 

Pour avancer vers sa réalisation il est nécessaire de renforcer les luttes 
actuelles contre les politiques antisociales, les populariser, les soutenir et 
à en initier d’autres, dans une perspective anticapitaliste et antiautoritaire, 
permettant le développement des capacités de gestion directe de l'ensemble de 
la population dans tous les domaines. 

L’éradication du fascisme passe par la lutte contre le capitalisme, l'Etat 
comme meilleur de ses gestionnaires, le racisme, le patriarcat et toutes les 
formes d’oppression et de domination. 

Contre la destruction programmée des retraites, la réduction annoncée des 
droits des chômeurs et précaires, la fermeture continue des services publics 
ou encore les plans de licenciements sans fin, il faut développer une 
confrontation sociale radicale dans les entreprises, les quartiers et auprès 
de la jeunesse. 

C'est encore une fois par la grève générale et le blocage de l'économie que 
nous pourrons faire échec aux logiques que nous imposent les classes dominantes 
au nom de la crise et donc éradiquer ce qui fait le terreau du fascisme.
 
Face aux problèmes sociaux et politiques, les élections n’offrent que des 
illusions de solution et de choix. 
Ce n'est pas dans les urnes que se résoudront la crise économique, les 
inégalités ou le développement du fascisme qui en est issu. Il faut donc 
refuser l’instrumentalisation des luttes à des fins électorales ou en vue 
d'asseoir l'hégémonie de partis institutionnels. 

Les nécessaires avancées sociales, seules véritables remparts face au 
développement du fascisme, se feront en développant les luttes et l'organisation 
dans nos entreprises et nos quartiers. 

Notre objectif est de construire une alternative au capitalisme et à l'Etat, 
fondée sur la réorganisation et la gestion directe de l'économie et de la 
société par les travailleuses et travailleurs, avec ou sans emploi.   
 
Face à la résurgence de la violence fasciste, il nous semble fondamental de 
développer l'autodéfense antifasciste populaire, collective et organisée, sans 
s'en remettre à l'Etat qui fait preuve d'une complaisance de fait envers les 
groupes fascistes, quelles que soient les proclamations d'intention qu'il puisse 
faire de manière opportuniste. 

De même, il est nécessaire d'informer et de sensibiliser largement sur le 
projet et les pratiques racistes et dictatoriales de l'extrême droite. 

Il est urgent de réagir, de se mobiliser et d'être solidaire lors des menées et 
violences fascistes, mais aussi d'empêcher le  développement de leur expression 
haineuse sur internet et dans les zones rurales ainsi que dans les petites 
villes où la présence des organisations antifascistes est moins importante.

- Face aux fascistes, il faut s'organiser pour les mettre hors d'état de  nuire
- Face aux capitalistes, il faut s'unir  pour détruire leur pouvoir et 
  redistribuer les richesses produites par les travailleuses et les travailleurs.
- Face à l’état, il faut prendre nos affaires en main et construire une 
  alternative sociale émancipatrice, égalitaire et libertaire.
  
  
