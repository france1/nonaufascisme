
.. index::
   pair: LUCIDE; Plateforme

.. _plateforme_lucide_2013:
   
=====================================================================================   
Plateforme du Collectif isérois LUCIDE LUtte Contre les IDées d'Extrême-droite (2013)  
=====================================================================================
   
.. seealso::

   - :ref:`rlf_isere_mars_2013`  
   - :ref:`lucide_pub` 
   
   
Texte
=====
   
Les idéologies, les discours, les exactions des droites extrêmes ne sont 
pas nés d’hier et un passé sinistre est là pour nous rappeler qu’il faut 
leur opposer, toujours, une action déterminée.

Le système capitaliste est aujourd’hui dans une crise si profonde qu’il 
ne croit pouvoir la résoudre, à son profit, qu’en imposant une austérité 
sans fin aux peuples, en Europe et dans le monde, par la réduction à une 
misère généralisée : bas salaires, précarité, attaques contre les retraites 
et les retraité-e-s pour demain, abaissement de la protection sociale, 
augmentation des taxes qui pèsent sur le pouvoir d’achat, et allègement 
des plus riches, ...

En France la rupture espérée n’est pas venue : des politiques d’austérité 
s’appliquent déjà. Il est inacceptable qu’aucun changement ne soit intervenu 
sur la question des Roms ou des sans-papiers : la chasse continue !

Ce n’est pas le coût du travail le problème, c’est le coût du capital, 
avec les dividendes sans cesse croissants exigés par les actionnaires 
(15%, 20% parfois plus alors qu’il y a 30 ans on parlait de 2 à 3%). 

Oui, les vrais assisté-e-s, ce sont les grands capitalistes dont les 
entreprises sont bénéficiaires d’exonérations massives (30 milliards 
d’euros en 2011), auxquelles s’ajoute la fraude fiscale pour 45 milliards, 
les niches fiscales pour 66 milliards.

Les peuples acceptent de moins en moins ces politiques (et ces politicien-ne-s)

Grande est la tentation d’utiliser les droites extrêmes, en France lepéniste 
et autres "décomplexées", héritières du fascisme et du spectre national 
socialiste.

Leurs méthodes sont toujours les mêmes : démagogie et mensonges, torsion 
de la vérité, division des victimes du capital, stigmatisations et 
discriminations tous azimuts.

Contrairement aux discours ambiants relayés par certains media, les 
responsables de la dégradation de nos conditions de vie ne sont pas les 
musulman-e-s, les immigré-e-s, les femmes, les LGBT, …, mais les capitalistes 
qui licencient à tour de bras, les gouvernements qui appliquent des 
politiques antisociales.

Les organisations signataires mesurent la gravité des récentes agressions 
commises dans toute la France : contre des jeunes militant-e-s communistes, 
contre des militant-e-s féministes, contre des militant-e-s antifascistes, 
et décident d’opposer un front de large union pour agir ensemble. 

Les violences verbales et physiques, exactions multiples contre les biens 
et les personnes qui ne pensent pas comme eux sont inacceptables.

Les fascismes, quel que soit leur camouflage, sont résistibles, pour peu 
que toutes les forces démocratiques, politiques, syndicales, associatives, 
que toutes celles et ceux qui en prennent conscience se rejoignent et 
opposent un front de lutte sans faille, pour juguler les forces mortifères 
des droites extrêmes, lepéniste et autres, avant qu’elles ne gagnent 
encore du terrain.

Le temps vient où aucun-e citoyen-ne ne pourra plus dire "je ne savais pas", 
ne pourra plus rester en dehors de la lutte déterminée contre l’extrême droite.

C’est pourquoi les organisations signataires décident :

- de créer un front de mise en alerte et de préparation d’actions massives 
  de riposte
- de prendre toute initiative commune pour développer la prise de conscience 
  la plus large contre l’utilisation des droites extrêmes par le capitalisme 
  destructeur.
- de lutter ensemble pour l’égalité des droits et la justice sociale

Février 2013


Premiers signataires isérois
-----------------------------

- Alternatifs, 
- Alternative Libertaire, 
- Association Départementale des Déportés et Internés, 
- Résistants et Patriotes, 
- Centre Information Inter-Peuples, 
- CGT-38, 
- Gauche Unitaire, 
- JC, 
- NPA, 
- Osez le Féminisme, 
- PAG38, 
- PCF, 
- Parti de Gauche , 
- PRCF, Sud-Solidaires, 
- SOS Racisme, 
- RLF-Ras L' Front  Isère, 
- Union Nationale Lycéenne , 
- et citoyen-ne-s militant-e-s, …   



   
   
