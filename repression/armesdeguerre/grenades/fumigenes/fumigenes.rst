.. index::
   pair: Grenades ; Lacrymogènes Fumigènes

.. _grenades_lacrymo:

=================================================================================================================================================================
Grenades Lacrymogènes Fumigènes **utilisation interdite en conflits armés** par la Convention sur l’interdiction des Armes Chimiques du 13 janvier 1993 à Paris
=================================================================================================================================================================

.. seealso::

   - https://maintiendelordre.fr/grenades-lacrymogenes-fumigenes/
   - :ref:`glif4`


.. contents::
   :depth: 3

Description
=============

:Source: https://maintiendelordre.fr/grenades-lacrymogenes-fumigenes/


Les grenades lacrymogènes sont des grenades libérant des fumigènes à effet
lacrymogène à travers plusieurs palets allant dans tous les sens pour couvrir
une zone maximale.
Construites par différents constructeurs, en différentes tailles et calibres
elles disposent chacune de caractéristiques techniques propres (nombre de palets,
pourcentage de CS, surface etc).

Elles sont majoritairement utilisées à la main ou à l’aide d’un lanceur COUGAR
ou PennArm, seule la MP7 Commando sort du lot avec son propre système de
propulsion à 100m.

Aujourd’hui toutes les grenades utilisées ont comme agent lacrymale du
2-Chlorobenzylidène malonitrile (appelé aussi CS) mais Alsetex a breveté
un fumigène au piment OC sensé être plus efficace et moins “toxique”.

Leur utilisation est interdite en conflits armés par la Convention sur
l’interdiction des Armes Chimiques du 13 janvier 1993 à Paris.

- 7940 Grenades utilisées par les CRS le 1er décembre 2018 à Paris
- 17.544.153 Euros dépensés en mai 2018 pour l'achat de nouvelles grenades
  et DPR pour une durée de 4 ans


Les effets au très court terme sont incapacitant : irritation des yeux et du
système respiratoire.
Des brûlures sur la peau ont déjà été constatées et des problèmes au plus
long terme sont suspectés comme des  possibles traces de cyanure ou un
dérèglement du cycle menstruel.

Cependant aucune étude médicale complète n’a permis d’identifier des problèmes
de santé au long terme.

Effets détaillés :

- gênes respiratoires
- nausées, vomissements
- irritation des voies respiratoires
- irritation des voies lacrymales et des yeux
- spasmes
- douleurs thoraciques
- dermatites, allergies.

Les effets sont accentués par temps chaud et humide.


À forte dose:

- l’effet le plus fréquent reste les brûlures pouvant aller jusqu’au second degré
- l’irritation oculaire peut parfois se compliquer de lésions de la cornée
  ou d’hémorragies du vitré
- nécrose des tissus dans les voies respiratoires
- nécrose des tissus dans l’appareil digestif
- œdèmes pulmonaires
- hémorragies internes (hémorragies des glandes surrénales)
- dégradation des produits en d’autres substances toxiques (cyanure et thiocyanate).

