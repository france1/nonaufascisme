.. index::
   pair: Lanceurs de Balles de Défense (LBD) ; Lanceurs
   pair: Lanceurs de Balles de Défense ; LBD
   pair: LBD ; LBD40


.. _lanceurs_lbd:

====================================================================================
**Lanceurs de Balles de Défense (LBD)** Le LBD 40 est classé comme *arme de guerre*
====================================================================================

.. seealso::

   - https://fr.wikipedia.org/wiki/Lanceur_de_balles_de_d%C3%A9fense


.. figure:: angouleme_2020/fier_de_lui.jpeg
   :align: center

   https://twitter.com/davduf/status/1222912372534317057?s=20


.. contents::
   :depth: 3

Description
============

*Lanceur de balles de défense* (LBD) est un terme de l'administration française
désignant une *arme sublétale* utilisant un projectile conçu pour se déformer/s'écraser
à l’impact et limiter le risque de pénétration dans un corps vivant, mais avec
une puissance d’arrêt suffisante pour dissuader ou arrêter un individu.

Ce type d'arme a été adopté par la Police nationale et des polices municipales,
puis par la Gendarmerie nationale et l'administration pénitentiaire, comme
armement intermédiaire entre la matraque et l'arme de poing.

Le premier LBD fut celui fabriqué et commercialisé sous la marque Flash-Ball
par le fabricant Verney-Carron.

Un lanceur de balles de défense est défini comme *arme de force intermédiaire* (AFI),
au même titre que le *pistolet à impulsions électriques* (PIE) et la
*grenade de désencerclement* (GMD/DBD/DMP).

Selon certaines sources, l'usage de LBD peut présenter à courte distance des
effets traumatiques dont la sévérité peut entraîner des **lésions graves pouvant
être irréversibles voire mortelles**.

Dans le cadre de son utilisation par les forces de l'ordre, le LBD a causé des
blessures : une personne est décédée et vingt-trois autres ont perdu l'usage
d'un œil en France entre 2004 et 2019, après avoir été touchées au visage
par un tir de LBD.

Le LBD 40
==========

Depuis début 2009, les forces de l’ordre françaises sont également équipées
d’un lanceur de balles de défense 40 mm fabriqué par l'armurier suisse
Brügger & Thomet, le B&T LL06, appelé LBD 40 en France, une version non
létale du lance-grenades B&T GL06.

Il s’agit d’un lanceur de munition de 40 mm de diamètre qui utilise des
projectiles non-sphériques afin d’assurer précision et puissance d’arrêt
de 25 à 50 mètres.

Son exportation est régit par la loi sur le contrôle des biens (LCB) et la loi
fédérale sur le matériel de guerre (LFMG).

En France, ce lance-grenades est classé comme arme de « catégorie A »
soit comme *matériel de guerre*

Nouveaux LBD
===============

Fin 2018, le ministère de l’Intérieur lance un appel d'offres pour la fourniture
de plusieurs centaines de lanceurs de balles de défense (LBD).

Le 27 novembre 2019, deux PME françaises remportent le marché.

Le groupe Rivolier livrera des lanceurs à six coups, soit 180 LBD et leur
équipement pour un montant de 727 450 euros.

Le second marché, remporté par Alsetex, concerne la fourniture de 1280
lanceurs de balles de défense mono-coup pour 1,638 million d’euros, ainsi
que 270 lanceurs à quatre coups et 180 lanceurs à six coups.

